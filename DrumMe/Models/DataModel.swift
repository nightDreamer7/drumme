//
//  DataModel.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

// MARK: - KB
struct KreatrBeat: Codable {
    let id: Int
    let menuTitle: String
    let headerTitle: String?
    let subheaderTitle: String?
    let headerImageURL: String?
    let backgroundImageURL: String?
    let trackList: [Track]?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case menuTitle
        case headerTitle
        case subheaderTitle
        case headerImageURL
        case backgroundImageURL
        case trackList
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: CodingKeys.id)
        menuTitle = try container.decode(String.self, forKey: CodingKeys.menuTitle)
        headerTitle = try container.decodeIfPresent(String.self, forKey: CodingKeys.headerTitle)
        subheaderTitle = try container.decodeIfPresent(String.self, forKey: CodingKeys.subheaderTitle)
        headerImageURL = try container.decodeIfPresent(String.self, forKey: CodingKeys.headerImageURL)
        backgroundImageURL = try container.decodeIfPresent(String.self, forKey: CodingKeys.backgroundImageURL)
        
        // Decoding track list.
        var tracks = [Track]()
        if let dictionary = try? container.decode([String: [Loop]].self, forKey: CodingKeys.trackList) {
            
            for key in dictionary.keys {
                
                if let trackType = TrackType.fromString(key),
                    let loops = dictionary[key] {
                    let track = Track(type: trackType, loops: loops)
                    tracks.append(track)
                }
            }
        }
        trackList = tracks
    }
}

struct Track: Codable {
    let type: TrackType // Drums, Bass, etc..
    let loops: [Loop]   // Array of all loops.
}

struct Loop: Codable {
    let activeImage: String
    let inactiveImage: String
    let loopURL: String
    let fileName: String
}

enum TrackType: String, Codable, CaseIterable {
    case drums
    case bass
    case melody
    case fx
    
    var displayName: String {
        switch self {
        case .drums:   return "Drums"
        case .bass:    return "Bass"
        case .melody:  return "Melody"
        case .fx:      return "Fx"
        }
    }
    
    static func fromString(_ string: String?) -> TrackType? {
        guard let string = string else { return nil }
        return TrackType(rawValue: string) ?? nil
    }
}

