//
//  DrumLineTableViewCell.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

    import UIKit

    protocol TrackTableViewCellDelegate: class {
        func loopButtonTapped(in cell: DrumLineTableViewCell, at index: Int)
        func loopButtonUnselected(in cell: DrumLineTableViewCell, at index: Int)
    }
    
    class DrumLineTableViewCell: UITableViewCell {
        
        @IBOutlet weak var trackTypeLabel: UILabel!
        
        // NOTE: Button tags start from index 1, to avoid other views with 0 tag.
        @IBOutlet weak var leftButton: UIButton!
        @IBOutlet weak var centerButton: UIButton!
        @IBOutlet weak var rightButton: UIButton!
        
        weak var delegate: TrackTableViewCellDelegate?
        
        // Currently selected button index.
        var selectedIndex: Int?
        
        // Array of track buttons.
        private var trackButtons: [UIButton] {
            return [leftButton, centerButton, rightButton]
        }
        
        // MARK: - Lifecycle API
        
        override func awakeFromNib() {
            super.awakeFromNib()
            setupView()
        }
        
        // MARK: - Public API
        
        func setupWith(_ track: Track) {
            unselectButtons()
            trackTypeLabel.text = track.type.displayName.uppercased()
            trackTypeLabel.sizeToFit()
            
            // Naming buttons.
            for i in 0..<trackButtons.count {
                let title = track.type.displayName + " \(i)"
                trackButtons[i].setTitle(title, for: .normal)
            }
        }
        
        // MARK: - Private API
        
        /// Setups initial values for the screen.
        private func setupView() {
            // Rotating track type label sideways.
            // NOTE: (-CGFloat.pi / 2 rotates sideways from bottom to top)
            let rotationDegree = -CGFloat.pi / 2
            trackTypeLabel.transform = CGAffineTransform(rotationAngle: rotationDegree)
            trackTypeLabel.center = CGPoint(x: trackTypeLabel.center.x, y: trackTypeLabel.center.y + 20)
            selectionStyle = .none
            
            trackButtons.forEach { $0.backgroundColor = .black
                $0.layer.cornerRadius = 20
                $0.layer.borderWidth = 5
                $0.layer.borderColor = UIColor.lightGray.cgColor
            }
        }
        
        /// Handles selection of the buttons logic.
        private func selectButton(_ button: UIButton) {
            // Unmarking currently selected button if any.
            unselectButtons()
            
            // User selected already highlighted button.
            guard selectedIndex != button.tag else {
                selectedIndex = nil
                delegate?.loopButtonUnselected(in: self, at: button.tag)
                return
            }
            
            // Marking selected button
            selectedIndex = button.tag
            updateState(for: button, isSelected: true)
            
            // Button tags start from index 1 to avoid views with 0 tags.
            let buttonIndex = button.tag - 1
            delegate?.loopButtonTapped(in: self, at: buttonIndex)
        }
        
        private func unselectButtons() {
            if let selectedIndex = selectedIndex,
                let button = viewWithTag(selectedIndex) as? UIButton {
                
                updateState(for: button, isSelected: false)
            }
        }
        
        /// Handles visual update for buttons.
        private func updateState(for button: UIButton, isSelected: Bool) {
            
            button.backgroundColor = isSelected ? .red : .black
        }
        
        // MARK: - IBActions
        
        @IBAction func buttonTapped(_ sender: UIButton) {
            selectButton(sender)
        }
        
}
