//
//  BeatCollectionViewCell.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

class BeatCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [weak self] (timer) in
            self?.setSelected()
        }
    }

    private func setupViews() {
        self.imageView.layer.cornerRadius = 20
        self.imageView.layer.borderColor = UIColor.black.cgColor
        self.imageView.layer.borderWidth = 7
    }
    
    func setSelected() {
        if self.isSelected {
             self.imageView.backgroundColor = UIColor.red
             self.titleLabel.textColor = UIColor.white
        } else {
             self.imageView.backgroundColor = UIColor.white
             self.titleLabel.textColor = UIColor.black
        }
    }
}
