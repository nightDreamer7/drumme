//
//  RecorderViewController.swift
//  DrumMe
//
//  Created by Alex on 8/14/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit
import AVFoundation

class RecorderViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var player = AVAudioPlayer()
    private var selectedIndex = 3
    let titlesArray = ["NightLife", "Moonlight", "Robbery"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        var beatName = ""
        var extentionString = "mp3"
        switch selectedIndex {
        case 3:
            break
        case 0:
            beatName = titlesArray[0]
            extentionString = "mp3"
        case 1:
            beatName = titlesArray[1]
            extentionString = "wav"
        case 2:
            beatName = titlesArray[2]
            extentionString = "mp3"
        default:
            break
        }
        guard let url = Bundle.main.url(forResource: beatName, withExtension: extentionString) else {return}
        do {
            player = try! AVAudioPlayer(contentsOf: url)
            player.enableRate = true
            player.rate = 1.0
        } catch {
            print("error")
        }
        player.play()
    }
}

extension RecorderViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.tableView.rowHeight))
        let label = UILabel(frame: CGRect(x: cell.center.x, y: cell.center.y, width: cell.contentView.bounds.width, height: cell.contentView.bounds.height))
        let index = indexPath.item
        label.text = "\(titlesArray[index]) completed"
        label.contentMode = .center
        label.textAlignment = .natural
        cell.addSubview(label)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.item
        self.selectedIndex = index
    }
}

// MARK: - UITableViewDelegate
extension RecorderViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowCount = CGFloat(integerLiteral: 3)
        let rowHeight = ceil(tableView.frame.height / rowCount)
        return rowHeight
    }
}
