//
//  ViewController.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Image view for navigation bar setup
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "logo")
        
        // Settings for navigation bar
        let whiteNavBarAppearance = UINavigationBar.appearance(whenContainedInInstancesOf: [CustomNavigationController.self])
        whiteNavBarAppearance.barTintColor = .white
        whiteNavBarAppearance.tintColor = .black
        whiteNavBarAppearance.addSubview(imageView)
    }
}
