//
//  BeatConstructorViewController.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit
import AVFoundation
    class BeatConstructorViewController: UIViewController {
        
        // MARK: - Outlets
        
        @IBOutlet weak var tableView: UITableView!
        @IBOutlet weak var nextButton: UIButton!
        
        // MARK: - Private properties
        
        private var loopPlayerManager = LoopPlayerManager()
        
        fileprivate var kr38rBeat: KreatrBeat?
        var counter = 0
        var player = AVAudioPlayer()
        // MARK: - Lifecycle API
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setupView()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            counter = 0
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            loopPlayerManager.mutePlayers()
        }
        
        override var prefersStatusBarHidden: Bool {
            return true
        }
        
        // MARK: - Public API
        
        func setupWith(_ kr38rBeat: KreatrBeat) {
            self.kr38rBeat = kr38rBeat
        }
        
        // MARK: - Private API
        
        /// Setups screen.
        private func setupView() {
            nextButton.layer.borderColor = UIColor.black.cgColor
            nextButton.layer.borderWidth = 3
            loopPlayerManager.setSession()
            let nib = UINib.init(nibName: "DrumLineTableViewCell", bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: "DrumLineTableViewCell")
        }
        
        // DEBUG not optimzed, move all to video mix model.
        private func showTrackListViewController() {
           // let url = URL(fileURLWithPath: path)
            let sb = UIStoryboard(name: "BeatConstructorStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "RecorderViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        // MARK: - Actions
        
        @IBAction func nextButtonPressed(_ sender: UIButton) {
            DispatchQueue.main.async {
                self.showHUD(progressLabel: "Your beat is loading")
            }
            guard counter < 1 else { return }
            counter += 1
            loopPlayerManager.mutePlayers()
           // loopPlayerManager.mixTracks { path in
                DispatchQueue.main.async {
                   // self.dismissHUD(isAnimated: true)
                    self.showTrackListViewController()
              //  }
            }
        }
        
        @IBAction func backButtonPressed(_ sender: UIButton) {
            loopPlayerManager.mutePlayers()
            navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - UITableViewDataSource
    extension BeatConstructorViewController: UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return kr38rBeat?.trackList?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            guard let trackList = kr38rBeat?.trackList else { return UITableViewCell() }
            
            if indexPath.row < trackList.count,
                let resultCell = tableView.dequeueReusableCell(withIdentifier: "DrumLineTableViewCell") as? DrumLineTableViewCell {
                resultCell.delegate = self
                resultCell.setupWith(trackList[indexPath.row])
                
                return resultCell
            }
            
            return UITableViewCell()
        }
    }
    
    // MARK: - UITableViewDelegate
    extension BeatConstructorViewController: UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let rowCount = CGFloat(kr38rBeat?.trackList?.count ?? 0)
            let rowHeight = ceil(tableView.frame.height / rowCount)
            return rowHeight
        }
    }
    
    // MARK: - TrackTableViewCellDelegate
    extension BeatConstructorViewController: TrackTableViewCellDelegate {
        
        func loopButtonTapped(in cell: DrumLineTableViewCell, at index: Int) {
            guard let indexPath = tableView.indexPath(for: cell),
                let tracks = kr38rBeat?.trackList
                else { return }
            
            let track = tracks[indexPath.row]
            loopPlayerManager.play(track, at: index)
        }
        
        func loopButtonUnselected(in cell: DrumLineTableViewCell, at index: Int) {
            guard let indexPath = tableView.indexPath(for: cell),
                let tracks = kr38rBeat?.trackList
                else { return }
            
            let track = tracks[indexPath.row]
            loopPlayerManager.stop(track)
        }
    }
    
    extension BeatConstructorViewController {
        
        func showHUD(progressLabel:String){
//            DispatchQueue.main.async{
//                let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
//                progressHUD.label.text = progressLabel
//            }
        }
        
        func dismissHUD(isAnimated:Bool) {
//            DispatchQueue.main.async{
//                MBProgressHUD.hide(for: self.view, animated: isAnimated)
//            }
        }
}
