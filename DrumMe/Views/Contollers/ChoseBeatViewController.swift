//
//  ChoseBeatViewController.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

class ChooseTrackViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var kr38rBeats: [KreatrBeat]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        nextButton.layer.borderColor = UIColor.black.cgColor
        nextButton.layer.borderWidth = 3
        collectionView.register(UINib(nibName: "BeatCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BeatCollectionViewCell")
        collectionView.reloadSections(IndexSet(integer: 0))
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        guard
            let beatConstructorViewController = UIStoryboard.beatConstructorViewController,
            var indexPath = self.collectionView.indexPathsForSelectedItems?.first,
            let kr38rBeats = kr38rBeats, indexPath.row < kr38rBeats.count
            else { return }
        
        beatConstructorViewController.setupWith(kr38rBeats[indexPath.row])
        navigationController?.pushViewController(beatConstructorViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return kr38rBeats?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeatCollectionViewCell", for: indexPath) as? BeatCollectionViewCell {
            cell.titleLabel.text = kr38rBeats?[indexPath.item].menuTitle
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionView.ScrollPosition.top)
    }
    
    // MARK: - Helpers
    
    private func loadData() {
        if let url = Bundle.main.url(forResource: "data_model", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                kr38rBeats = try decoder.decode([KreatrBeat].self, from: data)
                
            } catch {
                print("error:\(error)")
            }
        }
    }
}

extension ChooseTrackViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.frame.width / 2.08, height: collectionView.frame.width / 2.08)
    }
}
