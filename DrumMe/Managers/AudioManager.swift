//
//  AudioManager.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import Foundation
import AVFoundation

class AudioManager {
    
    static let shared = AudioManager()
    var audioSession = AVAudioSession.sharedInstance()
    var bluetoothConnected = false
    
    // MARK: - Public API
    // Enables audio output to bluetooth devices.
    func enableBluetoothDevice(session: AVAudioSession) {
        let output = session.currentRoute.outputs[0]
        do {
            // Sets connected device as output player device
            try session.setPreferredInput(output)
            // Audio session settings
            try session.setActive(false)
            try session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: .allowBluetooth)
            try session.setActive(true)
            
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] (timer) in
                let output = session.currentRoute.outputs[0]
                if output.portType == AVAudioSession.Port.bluetoothA2DP ||
                    output.portType == AVAudioSession.Port.bluetoothHFP ||
                    output.portType == AVAudioSession.Port.bluetoothLE {
                    self?.bluetoothConnected = true
                } else {
                    self?.bluetoothConnected = false
                }
            }
        }
        catch {
            fatalError("Error cathed during audio session setup")
        }
    }
}
