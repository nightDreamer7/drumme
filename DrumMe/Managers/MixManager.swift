//
//  MixManager.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import Foundation
import AVFoundation

@objc class MixManager: NSObject {
    
    // MARK: - Class API
    
    /// Returns a path for merged track location.
    @objc static var mergedAudioTrackPath: String? {
        let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        if let audioURL = documentDirectoryURL.appendingPathComponent("FinalAud.m4a") as NSURL? {
            return audioURL.path
        }
        
        return nil
    }

    /**
     Merges audio tracks into a single file.
     - parameter tracks: a string array containing file names.
     - parameter exportPath: a path where tracks will be exported to.
     */
    static func mergeAudioTracks(_ tracks: [URL], exportPath: String?, completion: Bool) {
        guard let exportPath = exportPath else { return }
        
        let composition = AVMutableComposition()
        
        // Inserting tracks into composition.
        for trackUrl in tracks {
            let audioAsset = AVURLAsset(url: trackUrl)
            
            let constantTrackLength: Double = 160.0
            let countLoop = round(constantTrackLength / audioAsset.duration.seconds)
            let audioTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            for multiplier in 0..<Int(countLoop) {
                try? audioTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: audioAsset.duration), of: audioAsset.tracks(withMediaType: AVMediaType.audio)[0], at: CMTime(seconds: audioAsset.duration.seconds * Double(multiplier), preferredTimescale: CMTimeScale(0.0)))
            }
        }
        
        let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)
        
        // If file exists removing it first.
        let exportURL = URL(fileURLWithPath: exportPath)
        if FileManager.default.fileExists(atPath: exportPath) {
            try? FileManager.default.removeItem(atPath: exportPath)
        }
        assetExport?.outputFileType = AVFileType.m4a
        assetExport?.outputURL = exportURL
        assetExport?.shouldOptimizeForNetworkUse = true
        assetExport?.exportAsynchronously(completionHandler:
            {
                switch assetExport!.status {
                case .failed, .cancelled, .unknown:
                    print("Failed exporting asset - \(String(describing: assetExport?.error))")
                case .completed:
                     print("Asset exported")
                default:
                    break
                }
        })
    }
}

