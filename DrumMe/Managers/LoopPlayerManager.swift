//
//  LoopPlayerManager.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import AVFoundation

class LoopPlayerManager {
    
    // MARK: - Private properties
    var audioSession = AVAudioSession.sharedInstance()
    private var drumsPlayer  :AVAudioPlayer?
    private var bassPlayer   :AVAudioPlayer?
    private var melodyPlayer :AVAudioPlayer?
    private var fxPlayer     :AVAudioPlayer?
    
    // Returns an array of all available players.
    private var players: [AVAudioPlayer?] {
        return [drumsPlayer, bassPlayer, melodyPlayer, fxPlayer]
    }
    
    // MARK: - Public API
    var beatPath             :URL?
    
    /// Mutes each audio player.
    func mutePlayers() {
        players.forEach {
            $0?.stop()
            $0?.currentTime = 0
        }
    }
    
    /// Plays all available tracks and prepares given one.
    func play(_ track: Track, at index: Int) {
        mutePlayers()
        if let path = Bundle.main.path(forResource: track.loops[index].fileName, ofType:nil) {
            let url = URL(fileURLWithPath: path)
            
            assignPlayerFor(track, withUrl: url)
        }
        
        preparePlayers()
        playAvailablePlayers()
    }
    
    /// Stops corresponding player.
    func stop(_ track: Track) {
        switch track.type {
        case .drums:  drumsPlayer?.stop();  drumsPlayer = nil
        case .bass:   bassPlayer?.stop();   bassPlayer = nil
        case .melody: melodyPlayer?.stop(); melodyPlayer = nil
        case .fx:     fxPlayer?.stop();     fxPlayer = nil
        }
    }
    
    /// Mixes enabled tracks and returns path for merged audio.
    func mixTracks(completion: @escaping (String) -> Void) {
        // Appending active tracks.
        var tracksArray: [URL] = []
        players.forEach {
            if let url = $0?.url {
                tracksArray.append(url)
            }
        }
        
        // Merging active tracks into single file.
        if !tracksArray.isEmpty {
            let exportURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            self.beatPath = exportURL.appendingPathComponent("resultmerge.m4a")! as URL
            let exportPath = exportURL.path
            MixManager.mergeAudioTracks(tracksArray, exportPath: exportPath, completion: true)
            }
        }
    
    // MARK: - Private API
    func setSession() {
        AudioManager.shared.enableBluetoothDevice(session: self.audioSession)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            if AudioManager.shared.bluetoothConnected == true {
                self.players.forEach({ (player) in
                    player?.volume = 0.3
                })
            } else {
                self.players.forEach({ (player) in
                    player?.volume = 20
                })
            }
        }
    }
    
    // Prepares available players to play.
    private func preparePlayers() {
        players.forEach {
            $0?.prepareToPlay()
            $0?.volume = 0.05
        }
    }
    
    // Starts available players.
    private func playAvailablePlayers() {
        players.forEach {
            $0?.play()
        }
    }
    
    private func assignPlayerFor(_ track: Track, withUrl url: URL) {
        var player: AVAudioPlayer?
        
        do {
            player = try AVAudioPlayer(contentsOf: url)
            // Negative number loops indefinitely until stopped.
            player?.numberOfLoops = -1
            
            switch track.type {
            case .drums:  drumsPlayer = player
            case .bass:   bassPlayer = player
            case .melody: melodyPlayer = player
            case .fx:     fxPlayer = player
            }
        } catch {
            print("🛑 Error assigning player")
        }
    }
}
