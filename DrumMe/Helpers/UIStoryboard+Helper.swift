//
//  UIStoryboard+Helper.swift
//  DrumMe
//
//  Created by Alex on 8/13/19.
//  Copyright © 2019 OverBlack. All rights reserved.
//

import UIKit

enum Storyboards: String {
    
    case loopSelection = "BeatConstructorStoryboard"
    
    var storyboard: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
    
}

enum KRViewControllerID: String {
    
    case loopSelection = "BeatConstructorViewController"
    
}

extension UIStoryboard {
    
    private func instantiateViewController(_ identifier: KRViewControllerID) -> UIViewController? {
        return instantiateViewController(withIdentifier: identifier.rawValue)
    }
    
    // MARK: - Loop Selection Storyboard VCs
    
    static var beatConstructorViewController: BeatConstructorViewController? {
        return Storyboards.loopSelection.storyboard.instantiateViewController(.loopSelection) as? BeatConstructorViewController
    }
}
